FROM nersc/ubuntu-mpi:14.04

ENV PATH=/usr/sbin:/sbin:/usr/bin:/usr/local/bin:/bin

RUN apt-get -qq update && \
    apt-get -y upgrade && \
    apt-get -y install curl git-core cmake dpkg-dev zlib1g-dev libssl-dev 

RUN cd /usr/local/src && wget https://www.python.org/ftp/python/2.7.13/Python-2.7.13.tgz
RUN cd /usr/local/src && wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-1.3.1.tar.gz
RUN cd /usr/local/src && wget https://github.com/cython/cython/archive/0.25.2.tar.gz
RUN cd /usr/local/src && wget https://github.com/numpy/numpy/archive/v1.9.2.tar.gz
RUN cd /usr/local/src; tar xvzf Python-2.7.13.tgz
RUN cd /usr/local/src/Python-2.7.13;./configure ; make CFLAGS=-fPIC ; make install
RUN cd /usr/local/src; tar xvzf mpi4py-1.3.1.tar.gz
RUN cd /usr/local/src/mpi4py-1.3.1; /usr/local/bin/python setup.py build --mpicc=/usr/local/bin/mpicc;/usr/local/bin/python setup.py install
RUN cd /usr/local/src; tar xvzf 0.25.2.tar.gz
RUN cd /usr/local/src/cython-0.25.2 && /usr/local/bin/python setup.py install
RUN cd /usr/local/src; tar xvzf v1.9.2.tar.gz
RUN cd /usr/local/src/numpy-1.9.2 && /usr/local/bin/python setup.py install
RUN cd /usr/local && rm -rf ./src/*

ADD helloworld.c /helloworld.c
ADD helloworld.py /app/helloworld.py

RUN mpicc /helloworld.c -o /app/hello

ENV PATH=/usr/bin:/usr/local/bin:/bin:/app
